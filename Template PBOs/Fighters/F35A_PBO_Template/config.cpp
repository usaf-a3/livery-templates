#include "macros.hpp"

/*
Macros Available:

    LETTER(a)
    LETTER_DARK(a)
    NUMBER(1)
    NUMBER_DARK(1)
    FY(03)          //Must be two digits
    FY_DARK(03)     //Must be two digits
*/

class CfgPatches
{
    class MyName_F35A_Livery //Customize
    {
        name = "My F35 Livery Name"; //Customize
        author = "Me"; //Customize
        url = "";
        requiredVersion = 1.60;
        requiredAddons[] = { "USAF_F35A_C" };
        units[] = {};
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Plane;
    class USAF_F35A: Plane
    {
        class TextureSources
        {
            class MyLivery //Customize
            {
                author = "Your Name"; //Customize
                displayName = "Your Livery Name"; //Customize
                textures[] = {
                    "\My_F35A_Liveries\f35a_myLivery_co.paa", //Customize
                    
                    // Do not remove these!
                    "","","","","","","","","","","","","","","",
                    
                    "", //"front_logo", Squadron logo
                    "", //"tail_art", (Banner above base letters)
                    
                    //"tail_logo",
                    // acc_ca, acc_dark_ca, aetc_ca, aetc_dark_ca
                    "usaf_main\data\tail\acc_ca.paa",
                    
                    // Base EG
                    LETTER(e),LETTER(g),
                    
                    // Squadron 58 FS
                    NUMBER(5), NUMBER(8), "", LETTER(f), LETTER(s),
                    
                    // Serial 08-0746
                    FY(08), NUMBER(0), NUMBER(7), NUMBER(4), NUMBER(6)
                };
                materials[] = {
                    // Select only one!
                    "usaf_f35a\data\f_35c_ext_1.rvmat" // Default
                    //"usaf_f35a\data\f_35c_ext_1_matte.rvmat" // Lower gloss, with trim
                    //"usaf_f35a\data\f_35c_ext_1_notrim.rvmat" // Normal gloss, no trim
                    //"usaf_f35a\data\f_35c_ext_1_notrim_matte.rvmat" // Lower gloss, no trim
                };
            };
        };
    };
};
