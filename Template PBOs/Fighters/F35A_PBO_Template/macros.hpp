#define QUOTE(var1) #var1

#define PATHTO_LETTER(var1) usaf_main\characters\##var1.paa
#define PATHTO_LETTER_DARK(var1) usaf_main\characters\##var1_dg.paa
#define LETTER(var1) QUOTE(PATHTO_LETTER(var1))
#define LETTER_DARK(var1) QUOTE(PATHTO_LETTER_DARK(var1))

#define PATHTO_NUMBER(var1) usaf_main\characters\numbers\n_##var1.paa
#define PATHTO_NUMBER_DARK(var1) usaf_main\characters\numbers\n_##var1_dg.paa
#define NUMBER(var1) QUOTE(PATHTO_NUMBER(var1))
#define NUMBER_DARK(var1) QUOTE(PATHTO_NUMBER_DARK(var1))

#define PATHTO_FY(var1) usaf_main\data\tail\fy\##var1.paa
#define PATHTO_FY_DARK(var1) usaf_main\data\tail\fy\##var1_d.paa
#define FY(var1) QUOTE(PATHTO_FY(var1))
#define FY_DARK(var1) QUOTE(PATHTO_FY_DARK(var1))
