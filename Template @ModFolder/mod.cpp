// https://community.bistudio.com/wiki/Arma_3_Mod_Presentation
// Customize

name		= "USAF Mod Livery Template - Change my name in mod.cpp!";								// Name of your mod
//picture 	= "\Samples_F\Data_01\Images\picture.paa"; 		// Picture displayed from the expansions menu. Optimal size is 2048x1024
//logoSmall	= "\Samples_F\Data_01\Logos\logo_small.paa";	// Display next to the item added by the mod
//logo		= "\Samples_F\Data_01\Logos\logo.paa";			// Logo displayed in the main menu
//logoOver	 = "\Samples_F\Data_01\Logos\logoOver.paa";		// When the mouse is over, in the main menu
//action	   = "https://example.org/my-mod-page";			// Website URL, that can accessed from the expansions menu
//tooltipOwned = "It's yours!";								// Tool tip displayed when the mouse is left over, in the main menu

// Color used for DLC stripes and backgrounds (RGBA)
//dlcColor[] =
//{
//	0.23,
//	0.39,
//	0.30,
//	1
//};
// Overview text, displayed from the extension menu
overview = "Unleash your creativity! Here goes the long description of your mod.";
//hideName	= 0;	// Hide the extension name
//hidePicture	= 0;	// Hide the extension menu
