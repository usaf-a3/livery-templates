### Table of Contents
1. **[Setup Arma 3 Tools](https://github.com/Fullerpj/United-States-Air-Force-Mod/wiki/Custom-Livery-Guide#1-setup-arma-3-tools)**
2. **[Setup Directories](https://github.com/Fullerpj/United-States-Air-Force-Mod/wiki/Custom-Livery-Guide#2-setup-directories)**
3. **[Editing the template](https://github.com/Fullerpj/United-States-Air-Force-Mod/wiki/Custom-Livery-Guide#3-editing-the-template)**
4. **[Exporting the Texture](https://github.com/Fullerpj/United-States-Air-Force-Mod/wiki/Custom-Livery-Guide#4-exporting-the-texture)**
5. **[Customizing the Config](https://github.com/Fullerpj/United-States-Air-Force-Mod/wiki/Custom-Livery-Guide#5-customizing-the-config)**
6. **[Mod.cpp/Mod Presentation](https://github.com/Fullerpj/United-States-Air-Force-Mod/wiki/Custom-Livery-Guide#6-modcppmod-presentation)**
7. **[Packing the PBO](https://github.com/Fullerpj/United-States-Air-Force-Mod/wiki/Custom-Livery-Guide#6-packing-the-pbo)**
8. **[(Optional) Setup test mission](https://github.com/Fullerpj/United-States-Air-Force-Mod/wiki/Custom-Livery-Guide#7-optional-setup-test-mission)**
9. **[Publish](https://github.com/Fullerpj/United-States-Air-Force-Mod/wiki/Custom-Livery-Guide#8-publish)**

***


### Introduction
This is an exhaustive guide on creating custom liveries using the USAF templates. It will take you through the entire process of creating a livery and customizing the template to get it into the game, which you can then upload to the steam workshop. It looks convoluted but once you're setup and understand the structure, you'll see it's pretty simple. We'll build a livery from the F-35A template as an example.

## 1. Setup Arma 3 Tools
Before starting, you will need to install the Arma 3 Tools so that you can convert textures into PAA format and build the PBO. It should already be in your steam library in the Tools section or if not, on the steam workshop:

https://store.steampowered.com/app/233800/Arma_3_Tools/

You shouldn’t need to set up a P drive or anything fancy if you’re only doing texture work. You’ll only use two small utilities in the Arma 3 Tools, TexView 2 and Addon Builder.

## 2. Setup Directories
In the Templates download, you have two folders, `Template PBOs` and `Template @ModFolder`.

### Template PBOs
Find the template PBO folder for the aircraft you'd like to work on (`Fighters\F35A_PBO_Template` for the F-35). This contains the config template you will customize.

Create a copy of this folder and name it something unique and without spaces, such as YourName_F35A_Custom. This is your **PBO name** and will be used in various places later so keep it handy.

![explorer_wknKJi6oHE](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/explorer_wknKJi6oHE.png)
![explorer_1kW2fUIrMS](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/explorer_1kW2fUIrMS.png)

### Template `@Modfolder`
When finished, mods are structured like this: `@[Modfolder]\addons\[PBOname].pbo`. When Arma sees this structure, it shows the mod in the launcher.

We'll set this up in the Arma 3 directory since it's easiest, but you could use any location you like.

First, create a copy of `Template @Modfolder`. Rename it to something descriptive but without spaces (this can cause headaches for server admins). This will be the initial name that appears on the Steam Workshop, though you can change that after publishing without changing the modfolder name.

![explorer_rBtnaFHxOZ](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/explorer_rBtnaFHxOZ.png)
![explorer_aQwqWNgFH4](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/explorer_aQwqWNgFH4.png)

Now copy this folder into your primary Arma 3 game directory. This can be found by right clicking on the game in your Steam library > Manage > Browse Local Files.

![explorer_j9GnEz4JNj](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/explorer_j9GnEz4JNj.png)

### Adding the watched folder to the Launcher
Before the Arma launcher can load the mod, it has to know where the mod is. Oddly enough, the launcher seems to not watch the Arma 3 folder by default, so you may not have this set.

Run the Arma 3 Launcher. Hit the `Options` button at the top right, and select `Launcher Options`. Switch to the `Mods Options` tab. Under `Watched Folders` hit `Add 'Arma 3' Folder`. You should then see your Arma 3 folder in the list:

![arma3launcher_8JgD56D6UWs](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/arma3launcher_8JgD56D6UWs.png)

Your mod will not appear in the launcher until you build the PBO later, but you are now primed to do so.

## 3. Editing the template
Open the desired template in your editor of choice. The F-35A template was in the PBO folder, so you now have a new copy to edit. `Template PBOs\Fighters\[Your PBO Name]\f35a_livery_co.psd`. If you're in Photoshop, you will see layers with different colors. 

![Photoshop_9L6wn9bLIM](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/Photoshop_9L6wn9bLIM.png)

* Colors
  * Red - Don’t modify these layers, they contain shading and detail necessary to the model.
  * Green - Hide/show these layers to suit the needs of your livery
  * Blue - Modify/paint these layers to make your livery

* Editable Layers
  * UV - This shows the layout of the 3D model on the texture to assist with lining up elements. Hide before exporting your image.
  * Top Decals - Show/hide USAF Logos, Warning labels, etc.
  * Mid Elements - Show/hide trim, nose paint, etc
  * Design Goes Here - Add and paint various layers of your paint scheme

## 4. Exporting the Texture
### Export the image
Save/export your image as a .tga file (24 bit). You can use any filename you like, but it must end in “_co”, just like the template. You will reference this filename later so keep it handy.

If you're doing multiple liveries, replace “myLivery” with the name of this livery.

![Photoshop_n0NRVNOC9K2](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/Photoshop_n0NRVNOC9K2.png)

### Open in Texture Viewer
Run the Arma 3 Tools. In the dialog, open TexView 2.

![Arma3Tools_mFnCxJ5pvb](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/Arma3Tools_mFnCxJ5pvb.png)

Inside of Texview 2/Texture Viewer, open your .tga file. You can either use File > Open or just drag your image from explorer into the Texview window.

### Save as PAA
Hit File > Save As or Ctrl+Shift+S.

Edit the filename, **replacing .tga with .paa**, and save.

![TexView_AvpInKwwmo](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/TexView_AvpInKwwmo.png)
![TexView_9M4leBmXK7](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/TexView_9M4leBmXK7.png)

If you have multiple liveries, rinse and repeat for each one.

Finally, if you weren't there already, copy your texture(s) into your PBO folder.

![explorer_akxMxFm7ur](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/explorer_akxMxFm7ur.png)

## 5. Customizing the Config
Still inside the PBO folder, open config.cpp in a text editor like notepad or notepad++. (examples are from atom)

### CfgPatches
Customize the CfgPatches class. At the bare minimum, you will want to change the highlighted values.

![atom_rpEiiy22bl2](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/atom_rpEiiy22bl2.png)

* **Class** - This _must_ be unique across all Arma mods that a player may use. A good starting point is [MyTag]_[ThisAircraft]_Liveries, which in my case would be Raynor_F35A_Liveries.
* **Name** - Pretty display name that will appear in the ingame addons menu, most people won’t see this.
* **Author** - Your name, most people won’t see this (here).

`requiredAddons[]` will be setup in the template beforehand, so don't change it.

Again this is just the bare minimum. See the BI Wiki on further use of CfgPatches: https://community.bistudio.com/wiki/CfgPatches

### CfgVehicles
The CfgVehicles entry is long but actually quite simple. 

### Class
* **class MyTag_MyLivery** - Change MyTag to your tag, and MyLivery to a unique name for this livery without spaces. The tag is good to include because this value must be unique among all possible liveries for this aircraft. example: (`Raynor_DarkCamo`)
* **author** - Your name. Will appear when selecting the livery.
* **displayName** - This livery's name in the list.
```c++
        class TextureSources
        {
            class MyTag_MyLivery
            {
                author = "Your Name";
                displayName = "Your Livery Name";
```

### textures[]
This is where the rubber meets the road. In Arma, models can have texture "slots". textures[] tells the game what texture is placed in each slot when this livery is loaded. Convention dictates the first slot is the "camo" or main body texture, but some vehicles have 2-3 fuselage textures to avoid one massive texture.

```c++
                textures[] = {
                    "\F35A_PBO_Template\f35a_myLivery_co.paa", //Customize
                    
                    // Do not remove these!
                    "","","","","","","","","","","","","","","",
                    
                    "", //"front_logo", Squadron logo
                    "", //"tail_art", (Banner above base letters)
                    
                    //"tail_logo",
                    // acc_ca, acc_dark_ca, aetc_ca, aetc_dark_ca
                    "usaf_main\data\tail\acc_ca.paa",
                    
                    // Base EG
                    LETTER(e),LETTER(g),
                    
                    // Squadron 58 FS
                    NUMBER(5), NUMBER(8), "", LETTER(f), LETTER(s),
                    
                    // Serial 08-0746
                    FY(08), NUMBER(0), NUMBER(7), NUMBER(4), NUMBER(6)
                };
```
Line by line breakdown:
* `"\F35A_PBO_Template\f35a_myLivery_co.paa"` - The primary camo texture. This path is `[PBO name]\[texture name]`. 

The rest of these can be left alone, or you can customize your squadron. 
* `"","","","",...` - These are texture slots used for other things like the screens in the cockpit. Do not remove these.
* `"", //"front_logo", Squadron logo` - On the F-35, the logo on the front intake, usually the squadron logo.
* `"", //"tail_art",` - Small banner above the base letters
* `"usaf_main\data\tail\acc_ca.paa",` - Which command the aircraft is under, you can see included options for acc/aetc.
* `LETTER(e),LETTER(g), // Base EG` - The base letters. Macros to our characters have been included to make things easier, but you could use your own if you want.
* `NUMBER(5), NUMBER(8), "", LETTER(f), LETTER(s), // Squadron 58 FS` - To leave a slot blank, just use an empty string.
* `FY(08), NUMBER(0), NUMBER(7), NUMBER(4), NUMBER(6)` - Aircraft serial. **Make sure you use two digits on the FY.** Note serial # is randomized at spawn by default.

### factions[]
```c++
factions[] = {"USAF","BLU_F"};
```
If you would like to limit your livery being available to only certain factions (sides), you can add this line. Otherwise it isn't needed.
You can find a list of factions here: https://community.bistudio.com/wiki/faction

### materials[]
```c++
                materials[] = {
                    // Select only one!
                    "usaf_f35a\data\f_35c_ext_1.rvmat" // Default
                    //"usaf_f35a\data\f_35c_ext_1_matte.rvmat" // Lower gloss, with trim
                    //"usaf_f35a\data\f_35c_ext_1_notrim.rvmat" // Normal gloss, no trim
                    //"usaf_f35a\data\f_35c_ext_1_notrim_matte.rvmat" // Lower gloss, no trim
                };
```

This section will list alternate materials, if any are available. Make sure to leave the default active even if you don't want to change it.
This option also allows you to make your own, if you wanted a custom smdi or something.

## 6. Mod.cpp/Mod Presentation
Lastly, inside your @modfolder, open mod.cpp in a text editor. At the very least, change the `name` field to the same name you plan to use on the workshop. This value is used by the Arma 3 launcher, so it should match the workshop to avoid confusion.

To really get a polished mod, you can further customize mod.cpp. BI has good documentation of this here: ![Arma 3: Mod Presentation](https://community.bistudio.com/wiki/Arma_3:_Mod_Presentation)

---

That's it! Your mod is ready to be packed.

## 7. Packing the PBO

### Addon Builder File List
Run Arma 3 Tools and select Addon Builder.

![oR3x3Xeace](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/oR3x3Xeace.png)

Hit Options. In the first text field `List of files to copy directly`, delete everything and copy and paste this entire line and hit OK:
```
*.p3d;*.pac;*.paa;*.rtm;*.sqf;*.sqs;*.bikb;*.fsm;*.wss;*.ogg;*.wav;*.fxy;*.csv;*.html;*.lip;*.txt;*.wrp;*.bisurf;*.xml;*.rvmat;*.hpp;*.jpg;*.h
```
### Source Directory
Set Addon Source Directory to your PBO folder. You can hit the ellipsis button on the right to browse, but I prefer to just copy and paste the path from the explorer top bar. You will know the path is valid when the border turns yellow.

![AddonBuilder_Mzj8IN2YQ6](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/AddonBuilder_Mzj8IN2YQ6.png)

### Destination Directory
The Destination Directory is the addons folder inside the `@Modfolder` you copied in step 1. 

![AddonBuilder_JKjn8Djeiq](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/AddonBuilder_JKjn8Djeiq.png)

Start with these settings:

![AddonBuilder_jkHnJxngi5](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/AddonBuilder_jkHnJxngi5.png)

### Send it!
Hit `Pack`.

If the build was successful, you should see it pop up in the launcher:

![arma3launcher_pqWkjIlgBw](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/arma3launcher_pqWkjIlgBw.png)

Load the mod and fire up the game. Go into the editor, drop the vehicle, and Right click on it > `Edit Vehicle Appearance` > Paintcan on the left. You should now see your livery in the list.

If the build failed or you get a crash or error, check the error and triple check all your paths. It is very easy to make a mistake there.

![arma3_x64_UzwnL5Rs1i](https://raw.githubusercontent.com/Fullerpj/United-States-Air-Force-Mod/master/images/arma3_x64_UzwnL5Rs1i.png)

## 8. (Optional) Setup test mission 
For fast iteration, you can tell the game to immediately load a test mission at launch. 

* First, go into the editor and create a new mission on the Virtual Reality map. 
* Place down the aircraft that you're making a livery for.
* Select your livery under vehicle appearance and exit the garage.
* Place the camera with a good view of the livery. 
* Save the mission and exit the game.
* Back in the launcher, select `Parameters` on the left.
* Under `Basic`, check `Skip Logos at Startup`.
* Under `Basic`, find the `Mission file (editor)` parameter.
* Click the checkbox to enable it, and hit `...` to browse.
* Find your mission folder. It will be in `Documents > Arma 3 - Other Profiles > [Your name] > missions`.
* Double click `mission.sqm` within that folder.

You now have very few steps to make and preview changes.
* Change the image in photoshop and save the TGA.
* Throw it into Texview and save as .PAA (I just keep this open).
* Hit Pack in Addon Builder.
* Launch the game.

## 9. Publish
Your `@folder` is now ready to be published. BI has an extensive tutorial on doing so here: https://community.bistudio.com/wiki/Arma_3_Publisher

After you publish, you can go on the steam workshop and edit the title to something pretty, which does not change the modfolder name.

## That's all folks
Hopefully this guide has been clear and straightforward. If you have any feedback on it, please create an issue in this repo or let us know in discord.


